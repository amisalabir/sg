/* Voucher selection on Sale*/
$(document).ready(function(){

    /* Calculate total amouunt*/
   // $(function() {

        function getTotal(isInit) {

            var total = 0;
            var selector = isInit ? ".tot_amount" : ".tot_amount:checked";
            $(selector).each(function() {
                total += parseInt($(this).val());
            });
            //$("#tot_amount").val(sum.toFixed(3));

            if (total == 0) {
                $('#total1').val('');
            } else {
                $('#total1').val(total);
            }

        }

        $(".tot_amount").click(function(event) {
            getTotal();
        });
        getTotal(false);

   // });



   /* Honours Enable/Disable */
    var update_honours = function () {
        if ($("#honours").is(":checked")) {
            $('#honours_year').prop('disabled', false);
        }
        else {
            $('#honours_year').prop('disabled', 'disabled');

        }
    };

    $(update_honours);
    $("#honours").change(update_honours);

    /* Ma Enable/Disable */
    var update_ma = function () {
        if ($("#ma").is(":checked")) {
            $('#ma_year').prop('disabled', false);
        }
        else {
            $('#ma_year').prop('disabled', 'disabled');
        }
    };

    $(update_ma);
    $("#ma").change(update_ma);

    /* Mphil Enable/Disable */
    var update_mphil = function () {
        if ($("#mphil").is(":checked")) {
            $('#mphil_year').prop('disabled', false);
        }
        else {
            $('#mphil_year').prop('disabled', 'disabled');
        }
    };

    $(update_mphil);
    $("#mphil").change(update_mphil);


    /* Phd Enable/Disable */
    var update_phd = function () {
        if ($("#phd").is(":checked")) {
            $('#phd_year').prop('disabled', false);
        }
        else {
            $('#phd_year').prop('disabled', 'disabled');
        }
    };

    $(update_phd);
    $("#phd").change(update_phd);

    /*Enable disable Guest*/

    var update_guest = function () {
        if ($("#guest").is(":checked")) {
            $('#guest_1').prop('disabled', false);
            $('#guest_2').prop('disabled', false);
            $('#guest_3').prop('disabled', false);
            var totalguest=("#")
        }
        else {
            $('#guest_1').prop('disabled', 'disabled');$('#guest_1').prop('checked', false);
            $('#guest_2').prop('disabled', 'disabled');$('#guest_2').prop('checked', false);
            $('#guest_3').prop('disabled', 'disabled');$('#guest_3').prop('checked', false);

            getTotal(false);
        }
    };

    $(update_guest());
    $("#guest").change(update_guest);

    /*Enable disable Child*/
    var update_child = function () {
        if ($("#child").is(":checked")) {
            $('#child_1').prop('disabled', false);
            $('#child_2').prop('disabled', false);
            $('#child_3').prop('disabled', false);
        }
        else {
            $('#child_1').prop('disabled', 'disabled');$('#child_1').prop('checked', false);
            $('#child_2').prop('disabled', 'disabled');$('#child_2').prop('checked', false);
            $('#child_3').prop('disabled', 'disabled');$('#child_3').prop('checked', false);
            getTotal(false);
        }
    };

    $(update_child());
    $("#child").change(update_child);
    /*######################################*/

    /*Enable disable Infant*/
    var update_infant = function () {
        if ($("#infant").is(":checked")) {
            $('#infant_1').prop('disabled', false);
            $('#infant_2').prop('disabled', false);
            $('#infant_3').prop('disabled', false);
        }
        else {
            $('#infant_1').prop('disabled', 'disabled');$('#infant_1').prop('checked', false);
            $('#infant_2').prop('disabled', 'disabled');$('#infant_2').prop('checked', false);
            $('#infant_3').prop('disabled', 'disabled');$('#infant_3').prop('checked', false);
            getTotal(false);
        }
    };

    $(update_infant());
    $("#infant").change(update_infant);
    /*######################################*/

    /*Enable disable Spouse*/
    var update_spouse = function () {
        if ($("#spouse").is(":checked")) {
            $('#spouse_1').prop('disabled', false);
            $('#spouse_2').prop('disabled', false);
        }
        else {
            $('#spouse_1').prop('disabled', 'disabled');$('#spouse_1').prop('checked', false);
            $('#spouse_2').prop('disabled', 'disabled');$('#spouse_2').prop('checked', false);
            getTotal(false);
        }
    };

    $(update_spouse());
    $("#spouse").change(update_spouse);
    /*######################################*/


    /*Add Donor Ammount*/

    /*Add Donor Ammount Ended*/




});

