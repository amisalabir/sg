<?php
	include('header.php');

?>
	<!--<div class="bcumb" style="background-image: url('resource/img/banner.jpg')">
		<div class="overlay">
			<div class="container text-center">
				<div class="bcumbarea">
					<h4>Online Registration</h4>
					<p>Alumni needs enable you to harness the power of your alumni network. Whatever may be the need.</p>
					<a href="#" class="btn btn-default abttop">Let's See</a>
				</div>
			</div>
		</div>
	</div>-->
	<div class="">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<div class="registered">
						<div class="table-responsive box">
							<form class="well form-horizontal" action="#" method="post" id="contact_form">
								<h4 class="text-center A">The Chittagong University English Alumni Association</h2>
								<h5 class="text-center B">4th Alumni Reunion-2019</h3><hr/>
							<fieldset>
								<legend><center><font size="" class="C">(All Registered Member List)</font></center></legend>
								<p class="all text-left col-md-4">Total Member: <span class="count">2000</span></p>
								<table style="text-align: center; font-size: 13px;" class="table table-bordered">
									<thead>
									  <tr>
										<th style="text-align: center;">SL.</th>
										<th style="text-align: center;">Photo</th>
										<th style="text-align: center;">Name</th>
										<th style="text-align: center;">Member Type</th>
										<th style="text-align: center;">Batch</th>
										<th style="text-align: center;">Mobile</th>
										<th style="text-align: center;">Email</th>
										<th style="text-align: center;">Participants</th>
										<th style="text-align: center;">Amount</th>
										<th style="text-align: center;">Payment Status</th>
										<th style="text-align: center;">Action</th>
									  </tr>
									</thead>
									<tbody>
										<tr>
										  <td style="text-align: center;">14</td>
										  <td style="padding-left: 10px;">
											<img width="30px" src="uploads/017141300774059.PNG" class="img img-responsive img-circle" alt="name">
										  </td>
										  <td style="text-align: center;">Kazi Sala Uddin</td>
										  <td style="text-align: center;">Life Member</td>
										  <td style="text-align: center;">6th</td>
										  <td style="text-align: center;">01714130077</td>
										  <td style="text-align: center;">amisalabir@gmail.com</td>
										  <td style="text-align: center;">3 </td>
										  <td style="text-align: center;">5500 Tk.</td>
										  <td style="text-align: center;">Unpaid</td>
										  <td style="text-align: center;"><a target="_blank" href="#">View</a> &nbsp; <a href="#">Edit</a> </td>
										</tr>     										
									</tbody>
								</table>
							</fieldset>
						</form>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
<?php
	include('footer.php');
?>