<?php
	include('header.php');
?>
	<div class="bcumb" style="background-image: url('../resource/img/banner.jpg')">
		<div class="overlay">
			<div class="container text-center">
				<div class="bcumbarea">
					<h4>Alumni committee</h4>
					<p>Alumni needs enable you to harness the power of your alumni network. Whatever may be the need.</p>
					<a href="#" class="btn btn-default abttop">Let's See</a>
				</div>
			</div>
		</div>
	</div>
	<div class="commmitte">
		<div class="container text-center">
			<h4>Our Honorable Committee</h4>
			<div class="row">
				<div class="col-md-3">
					<div class="area">
						<img src="../resource/img/men.png" class="img img-responsive" alt="image"/>
						<div class="overlaytext">
							<strong>Bryan Watson</strong>
							<p>President</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="area">
						<img src="../resource/img/men.png" class="img img-responsive" alt="image"/>
						<div class="overlaytext">
							<strong>Bryan Watson</strong>
							<p>President</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="area">
						<img src="../resource/img/men.png" class="img img-responsive" alt="image"/>
						<div class="overlaytext">
							<strong>Bryan Watson</strong>
							<p>President</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="area">
						<img src="../resource/img/men.png" class="img img-responsive" alt="image"/>
						<div class="overlaytext">
							<strong>Bryan Watson</strong>
							<p>President</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="area">
						<img src="../resource/img/men.png" class="img img-responsive" alt="image"/>
						<div class="overlaytext">
							<strong>Bryan Watson</strong>
							<p>President</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="area">
						<img src="../resource/img/men.png" class="img img-responsive" alt="image"/>
						<div class="overlaytext">
							<strong>Bryan Watson</strong>
							<p>President</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="area">
						<img src="../resource/img/men.png" class="img img-responsive" alt="image"/>
						<div class="overlaytext">
							<strong>Bryan Watson</strong>
							<p>President</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="area">
						<img src="../resource/img/men.png" class="img img-responsive" alt="image"/>
						<div align="center" class="overlaytext">
							<strong>Bryan Watson</strong>
							<p>President</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="speech">
		<div class="container text-center">
			<h4>Some Speech About Us</h4>
			<div class="row">
				<div class="col-md-4">
					<div class="sarea">
						<img src="../resource/img/g2.png" alt="image" class="img simg"/>
						<div class="mbox">
							<img src="../resource/img/coma.png" alt="image" class="img img-responsive"/>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
						<div class="tbox col-md-6">
							<strong>Robert Albert</strong>
							<p>Co-Founder</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="sarea">
						<img src="../resource/img/g2.png" alt="image" class="img simg"/>
						<div class="mbox">
							<img src="../resource/img/coma.png" alt="image" class="img img-responsive"/>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
						<div class="tbox col-md-6">
							<strong>Robert Albert</strong>
							<p>Co-Founder</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="sarea">
						<img src="../resource/img/g2.png" alt="image" class="img simg"/>
						<div class="mbox">
							<img src="../resource/img/coma.png" alt="image" class="img img-responsive"/>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
						<div class="tbox col-md-6">
							<strong>Robert Albert</strong>
							<p>Co-Founder</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	include('footer.php');
?>