<?php
ob_start();
use Mpdf\Utils\UtfString;
include('menu.php');
include('header.php');
include('session.php');
include_once('printscript.php');

$objController = new App\Controller\Controller();

$objController->setData($_SESSION);
if(isset($_GET) || isset($_GET['email'])){
    $_GET['singleView']='Yes';
    $objController->setData($_GET);
}

$objToArrayProfile=$objController->objectToArray($objController->view($_GET));

//var_dump($objToArrayProfile);

$id=$objToArrayProfile['0']['id'];
$password=$objToArrayProfile['0']['password'];
$fullName=$objToArrayProfile['0']['fullName'];
$fatherName=$objToArrayProfile['0']['fatherName'];
$motherName=$objToArrayProfile['0']['motherName'];
$addressLine1=$objToArrayProfile['0']['addressLine1'];
$addressLine2=$objToArrayProfile['0']['addressLine2'];
$city=$objToArrayProfile['0']['city'];
$state=$objToArrayProfile['0']['state'];
$postcode=$objToArrayProfile['0']['postcode'];
$country=$objToArrayProfile['0']['country'];
$email=$objToArrayProfile['0']['email'];
$phoneNumber=$objToArrayProfile['0']['phoneNumber'];
$dob=$objToArrayProfile['0']['dob'];
$gender=$objToArrayProfile['0']['gender'];
$passport=$objToArrayProfile['0']['passport'];
$birthcert=$objToArrayProfile['0']['birthcert'];
$passport_file=$objToArrayProfile['0']['passport_file'];
$picture=$objToArrayProfile['0']['picture'];
$brc=$objToArrayProfile['0']['brc'];
$cv=$objToArrayProfile['0']['cv'];
$comment=$objToArrayProfile['0']['comment'];
$tmpPassword=$_SESSION['0']['tmp_password'];

$file = '../docx/country.txt';
// Open the file to get existing content
$countrylist = file_get_contents($file);
//$countrylist= include "../docx/country.txt";
?>
    <div class="">
        <div class="container">
            <div class="row">

            </div>
        </div>
    </div>
    <!-- Start About area -->
    <div id="about" class="about-area">
        <div class="container">
            <div class="Mycon">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Update Profile</h2>
                    </div>
                </div>
            </div>
            <?php
            $backbutton="";
            $editContent=<<<EDIT
    <form class="" method="POST" action="update.php" enctype="multipart/form-data">
             <input type="hidden" id="update" name="update" value="update">
            <div class="row">
        <div class="col-md-6">
          <div class="well form-horizontal">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Full Name</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="fullName" name="fullName" placeholder="Full Name" class="form-control" required="true" value="$fullName" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Father's Name</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="fatherName" name="fatherName" placeholder="Father's Name" class="form-control" required="true" value="$fatherName" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Mother's Name</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="motherName" name="motherName" placeholder="Mother's Name" class="form-control" required="true" value="$motherName" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Address 1</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="addressLine1" name="addressLine1" placeholder="Address Line 1" class="form-control" required="true" value="$addressLine1" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Address 2</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="addressLine2" name="addressLine2" placeholder="Address Line 2" class="form-control" required="true" value="$addressLine2" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">City</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="city" name="city" placeholder="City" class="form-control" required="true" value="$city" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">State/Province/Region</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="state" name="state" placeholder="State/Province/Region" class="form-control" required="true" value="$state" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Postal Code/ZIP</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="postcode" name="postcode" placeholder="Postal Code/ZIP" class="form-control" required="true" value="$postcode" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Country</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                        $countrylist
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Email</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span><input id="email" name="email" placeholder="Email" class="form-control" required value="$email" type="email"></div>
                                </div>
                            </div>
              <div class="form-group">
                                <label class="col-md-4 control-label">Phone Number</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span><input id="phoneNumber" name="phoneNumber" placeholder="Phone Number" class="form-control" required="true" value="$phoneNumber" type="text"></div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
        </div>
        <div class="col-md-6">
          <div class="well form-horizontal">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Date Of Birth</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span><input type="date" class="form-control" name="dob" id="dob" value="$dob" required="required"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Gender</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                        <select class="selectpicker form-control" name="gender" id="gender">
                                            <option>Select Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Passport Number</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-key"></i></span><input id="passport" name="passport" placeholder="Passport Number" class="form-control" required="true" value="$passport" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Birth Certificate No.</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-key"></i></span><input id="birthcert" name="birthcert" placeholder="Birth Certificate Number" class="form-control" required="true" value="$birthcert" type="text"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Type Of Visa</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group">
                                        <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                                        <select class="selectpicker form-control" name="tov" id="gender">
                                            <option>Select Visa</option>
                                            <option value="Student Visa">Student Visa</option>
                                            <option value="Business Visa">Business Visa</option>
                                            <option value="Work Visa">Work Visa</option>
                                            <option value="Visit Visa">Visit Visa</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Passport</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-file"></i></span><input id="passport_file" name="fileToUpload[0]" class="form-control file" required="true" value="" type="file"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Picture</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-file"></i></span><input id="picture" name="fileToUpload[1]" class="form-control file" required="true" value="" type="file"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Birth Reg. Certificate</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-file"></i></span><input id="brc" name="fileToUpload[2]" class="form-control file" required="true" value="" type="file"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Upload CV</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-file"></i></span><input id="cv" name="fileToUpload[3]" class="form-control file" required="true" value="" type="file"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Additional Comments</label>
                                <div class="col-md-8 inputGroupContainer">
                                    <div class="input-group"><span class="input-group-addon"><i class="fa fa-comment"></i></span><textarea class="form-control" rows="3" name="comments" id="comments"></textarea></div>
                </div>
                            </div>
                            <div class="text-right"><button name="submit"  type="submit" class="btn btn-primary">Update</button></div>
                        </fieldset>
                    </div>

        </div>

            </div>
        </form>
EDIT;
            $backbutton= " ";
            if($_SESSION['loginas']=='Admin') {

                echo $editContent;
               $backbutton= "<div align='center'><a href=\"applicants.php\" class=\"btn btn-primary\" role=\"button\">Go Back</a></div>";
            }
            if($_SESSION['loginas']=='User') {
                // $menus=$objController->objectToArray($obj->userMenus());
                echo $editContent;
               $backbutton= "<div align='center'><a href=\"home.php\" class=\"btn btn-primary\" role=\"button\">Go Back</a></div>";
            }

        echo $backbutton;


            ?>

        </div>
    </div>
    <!-- End About area -->
<?php
include('footer.php');
?>