<?php
	ob_start();
	include('header.php');
	include('session.php');

	if($_SESSION['loginas']=='Admin'){
	$allmembers=$objController->view();
	//   var_dump($allmembers);
}

?>
    <!-- Page Content -->
    <div class="container">
    	<div class="">
    		<style type="text/css">
    			#tablehead th{ text-align: center; }
    		</style>

		<table  border="1" width="100%" style="border-collapse: collapse; text-align: center; ">
			
			<tr id="tablehead">
				<th>ID</th><th>Photo</th><th>Name</th><th>DOB</th><th>Mobile</th><th>Actions</th>
			</tr>
			

			<?php
			foreach($allmembers as $singleMember){
				echo "
				<tr><td >$singleMember->id</td><td><img width='70px' src=\"uploads/$singleMember->picture\" class=\"img-circle\"></td><td>$singleMember->fullName</td><td>$singleMember->dob</td><td>$singleMember->phoneNumber</td>
				<td >
				<a class='btn btn-primary' href='profile.php?email=$singleMember->email'>View</a>
				<a class='btn btn-warning' href='edit.php?email=$singleMember->email'>Edit</a>
				<a class='btn btn-success' href='assessment.php?email=$singleMember->email'>Assessment</a>
				<a class='btn btn-danger' href='delete.php?deleteid=$singleMember->id'>Delete</a>
				</td>
			</tr>
				";
			}


			?>


		</table>

		</div>
	</div>	
    


<?php

	include('footer.php');
	include('footer_script.php');

?>
